import React from "react";
import { useChatMessages } from "../hooks/useChatMessages";
import MessageInput from "../components/MessageInput";
import MessageList from "../components/MessageList";

const Chat = ({ user }) => {
  const { messages, addMessage } = useChatMessages();
  return (
    <section className="section">
      <div className="container">
        <h1 className="title">Chatting as {user.name}</h1>
        <MessageList user={user} messages={messages} />
        <MessageInput onSend={addMessage} />
      </div>
    </section>
  );
};

export default Chat;
