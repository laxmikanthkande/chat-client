import React, { Component } from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import { getLoggedInUser, logout } from "./services/auth";
import Chat from "./views/Chat";
import Login from "./views/Login";
import NavBar from "./components/NavBar";
import client from "./graphql/client";

class App extends Component {
  state = { user: getLoggedInUser() };

  handleLogin(user) {
    this.setState({ user });
  }

  handleLogout() {
    logout();
    this.setState({ user: null });
  }

  render() {
    const { user } = this.state;
    if (!user) {
      return <Login onLogin={this.handleLogin.bind(this)} />;
    }
    return (
      <ApolloProvider client={client}>
        <NavBar onLogout={this.handleLogout.bind(this)} />
        <Chat user={user} />
      </ApolloProvider>
    );
  }
}

export default App;
