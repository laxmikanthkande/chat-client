import jwtDecode from "jwt-decode";
import env from "react-dotenv";

const accessTokenKey = env.ACCESS_TOKEN_KEY;
const loginUrl = `${env.API_URL}/login`;

const getUserFromToken = (token) => {
  console.log(jwtDecode(token));
  return jwtDecode(token);
};

export const getAccessToken = () => {
  return localStorage.getItem(accessTokenKey);
};

export const getLoggedInUser = () => {
  const token = getAccessToken();
  if (!token) {
    return null;
  }
  return getUserFromToken(token);
};

export const login = async (name, password) => {
  const response = await fetch(loginUrl, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({ name, password }),
  });
  if (!response.ok) {
    return null;
  }
  const { token } = await response.json();
  localStorage.setItem(accessTokenKey, token);
  return getUserFromToken(token);
};

export const logout = () => {
  localStorage.removeItem(accessTokenKey);
};
